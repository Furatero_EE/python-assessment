def chinese_puzzle(heads, legs):
	ns = 'No solutions!'
	for chicken in range(heads + 1):
		rabbit = heads - chicken
		if (2 * chicken) + (4 * rabbit) == legs:
			return chicken, rabbit
	return ns, ns