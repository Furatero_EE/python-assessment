def compute_magic_number(name):
	namelst = [x for x in name]
	number = 0
	for character in namelst:
		number += ord(character) - 96
	numstring = str(number)
	finalnum = 0
	while len(numstring) >= 2:
		for num in numstring:
			finalnum += int(num)
			numstring = str(finalnum)
	print(numstring)