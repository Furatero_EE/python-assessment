import os
import glob
import string
import unittest

from operator import itemgetter


def search_character(character):
	path = "/home/fuhrar/Python Assessment/python-assessment/marvel-wikia-data"
	extension = "csv"
	os.chdir(path)
	results = glob.glob('*.{}'.format(extension))
	final_lst = []
	display_lst = []

	results.sort()
	for result in results:
		with open(result) as decade:
			for index, line in enumerate(decade):
				lst = [z.strip() for z in line.split(",")]
				if character.lower() in lst[1].lower():
					final_lst.append(lst)
	
	for found in final_lst:
		pair_lst = []
		for index, value in enumerate(found):
			if index == 1 or index == len(found)-1 or index == len(found)-3:
				if index == len(found)-3:
					if value is '':
						pair_lst.append(0)
					else:
						pair_lst.append(int(value))
				else:
					pair_lst.append(value)
		display_lst.append(pair_lst)

	sorted(display_lst, key = itemgetter(1))
	for display in display_lst:
		print(display)
		yield display
		#return display


def compute_percentage_per_decade(decade):
	path = "/home/fuhrar/Python Assessment/python-assessment/marvel-wikia-data"
	extension = "csv"
	os.chdir(path)
	results = glob.glob('*.{}'.format(extension))
	total_people = 0
	living_people = 0
	dead_people = 0
	dictionary = {}

	results.sort()
	for result in results:
		if decade in result:
			with open(result) as value:
				for index, column in enumerate(value):
					if len(column)-4:
						#check if living
						if "living" in column.lower():
							living_people += 1
						else:
							dead_people += 1

						total_people += 1
			break

	dictionary["decade"] = decade
	dictionary["alive"] = living_people/total_people
	dictionary["dead"] = dead_people/total_people

	#print(dictionary["decade"])
	#print("alive: %.2f" % dictionary["alive"])
	#print("dead: %.2f" % dictionary["dead"])

	return dictionary


def is_good_function(some_function):
	def wrapper():
		some_function()

def is_bad_function(some_function):
	def wrapper():
		some_function()

def is_neutral_function(some_function):
	def wrapper():
		some_function()


class TestSearchCharacter(unittest.TestCase):
	def testSearchCharacter(self):
		query = search_character('captain')
		next(query)
		next(query)
		next(query)
		next(query)


class TestComputePercentagePerDecade(unittest.TestCase):
	def testCompute(self):
		query = compute_percentage_per_decade('1970')
		#query()


if __name__ == "__main__":
	unittest.main()