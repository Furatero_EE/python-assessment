def rotate_word(word, intshift):
	charlst = [x for x in word]
	newlst = []
	for char in charlst:
		num = ord(char) + intshift
		if num <= 96:
			 num = 122 - (96 - num)
		newlst.append(chr(num))
		#print(num)
	finalword = ''
	#print(newlst)
	for i in newlst:
		finalword += i
	print(finalword)