import re


def is_valid_password(password):
	#regex1 = r"^[A-Za-z0-9]((?=.*[A-Z])(?=.*[a-z])(?=.*\d)(?=.*[\@#$%_+=*()!?-])[A-Za-z0-9@#$%_+=*()!?-]{5,19})[@#$%_+=*()!?-]$"
	regex2 = r"^(?=.*[A-Z])(?=.*[a-z])(?=.*\d)(?=.*[\@#$%_+=*()!?-])(?=^[A-Za-z0-9]{6,18}[@#$%_+=*()!?-]$).{8,20}$"
	if re.match(regex2, password):
		return True
	else:
		return False

print(is_valid_password("1Aaaaaaa$"))