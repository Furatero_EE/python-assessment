from datetime import datetime
class Employee(object):
	def __init__(self, first_name, last_name, birthday, employment):
		self.first_name = first_name
		self.last_name = last_name
		self.birthday = birthday
		self.employment = employment
	def getFirstName(self):
		return self.first_name
	def getLastName(self):
		return self.last_name
	def getBirthday(self):
		return self.birthday
	def getEmployment(self):
		return self.employment
	def canBeTeamLead(self):
		today = date.today().strftime("%Y-%m-%d")
		todayyear, todaymonth, todayday = map(int, today.split('-'))
		year, month, day = map(int, self.employment.split('-'))
		if todayyear - year >= 5:
			return True
		else:
			return False
	@classmethod
	def from_input(cls):
		return cls(
			input("Enter first name: "),
			input("Enter last name: "),
			input("Enter birth date in YYYY-MM-DD format: "),
			input("Enter employment date in YYYY-MM-DD: ")
		)